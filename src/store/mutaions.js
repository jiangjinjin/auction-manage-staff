import {
	ZT_Info,
	Login,
	ReserveInfo,
	WaterMark,
	WaterMarkSub
} from './mutation-type'

const mutations = {
	[ZT_Info] (state, data) {
		state.ztInfo = {
			...state.ztInfo,
			...data
		}
	},
	[Login] (state, data) {
		state.isLogin = data
	},
	[ReserveInfo] (state, data) {
		state.reserveInfo = data
	},
	[WaterMark] (state, data) {
		state.waterMark[data.index] = data.list
	},
	[WaterMarkSub] (state, data) {
		state.waterMark[data.index][data.sub] = data.img
	}
}
export default mutations
