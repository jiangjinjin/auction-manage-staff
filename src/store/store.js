import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import mutations from './mutaions'
import getters from './getters'
import actions from './actions'

Vue.use(Vuex)

const state = {
	isLogin: false,
	ztInfo: {
		digest: '',
		name: '',
		telNo: '',
		uid: '',
		username: ''
	},
	reserveInfo: [],
	waterMark: []
}

const store = new Vuex.Store({
	state,
	getters,
	mutations,
	actions,
	modules: {
	},
	plugins: [createPersistedState({
		// 配置白名单
		paths: [
			'ztInfo',
			'isLogin'
		],
		storage: {
			getItem: key => wx.getStorageSync(key),
			setItem: (key, value) => wx.setStorageSync(key, value),
			removeItem: key => {}
		}
	})]
})

export default store
