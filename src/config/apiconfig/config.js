// 配置
export const ENV = process.env.NODE_ENV
export const DEV = 'development'
export const PRO = 'production'
export const devHost = 'https://ztfp-api-test.dward.cn'
export const proHost = 'https://api.cgmobile.com.cn'

const newBaseURLMapping = {
	[DEV]: devHost,
	[PRO]: proHost
}

export const host = `${newBaseURLMapping[String(ENV)]}`

export const appid = ''
export const appKey = ''

export default {}
