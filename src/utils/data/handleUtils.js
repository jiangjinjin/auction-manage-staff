import _ from 'lodash'

// 字符串数字格式化成千分位样式
export const commaFormat = (val) => {
	const newVal = String(val)
	if (_.isEmpty(newVal)) {
		return ''
	}
	const tmpVal = String(parseFloat(newVal).toFixed(2))
	const backVal = tmpVal.substr(tmpVal.indexOf('.'))
	const frontVal = tmpVal.substr(0, tmpVal.indexOf('.'))

	const reg = /(?=(?!(\b))(\d{3})+$)/g
	return frontVal.replace(reg, ',') + backVal
}

/**
 *
 * @param {*} num
 */
/* eslint-disable */
export const formatCurrency = (num) => {
	if (num) {
		num = num.toString().replace(/\$|\,/g, '')

		if (num === '' || isNaN(num)) { return 'Not a Number ! ' }

		const sign = num.indexOf('-') > 0 ? '-' : ''

		var cents = num.indexOf('.') > 0 ? num.substr(num.indexOf('.')) : ''
		cents = cents.length > 1 ? cents : ''

		num = num.indexOf('.') > 0 ? num.substring(0, (num.indexOf('.'))) : num

		if (cents === '') { if (num.length > 1 && num.substr(0, 1) === '0') { return 'Not a Number ! ' } } else { if (num.length > 1 && num.substr(0, 1) === '0') { return 'Not a Number ! ' } }
		for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
			num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3))
		}

		return (sign + num + cents)
	}
}
/* eslint-enable */

export default {}
