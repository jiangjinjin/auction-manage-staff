import {postFetch, getFetch} from '../http/api'

export const postGoodsInfo = async () => {
	const result = await postFetch({
		url: '/api/goods/findKCPage'
	})
	return result
}

// 员工负责看样列表
export const postGoodsList = async (params = {}) => {
	const result = await postFetch({
		url: '/api/cmsGoodsDate/findMaPage',
		data: params
	})
	return result
}

// 看样小结

export const postGoodsXJ = async (params = {}) => {
	const result = await postFetch({
		url: '/api/cmsGoodsDate/updateSimple',
		data: params
	})
	return result
}
// 预约时间参数

export const postGoodsRecordInfo = async (params = {}) => {
	const result = await postFetch({
		url: 'api/cmsOrders/findAll',
		data: params
	})
	return result
}

// 新建预约信息

export const newGoodsRecordInfo = async (params = {}) => {
	const result = await postFetch({
		url: 'api/cmsOrders/createTmp',
		data: params
	})
	return result
}

// 更新预约信息
export const upLoadGoodsRecordInfo = async (params = {}) => {
	const result = await postFetch({
		url: 'api/cmsOrders/update',
		data: params
	})
	return result
}

// 内页详情
export const subGoodsInfo = async (id) => {
	const result = await getFetch({
		url: 'api/cmsGoodsDate/findOne/' + id
	})
	return result
}
