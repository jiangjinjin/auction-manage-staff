
import store from '../store/store'
import {apiDeps} from '../http/api'
export const isZTlogin = () => {
	// 判断登录状态
	if (store.state.isLogin) {
		// 验证数据完整性
		if (store.state.ztInfo.digest && store.state.ztInfo.telNo && store.state.ztInfo.uid) {
			apiDeps['Access-Token'] = store.state.ztInfo.digest + ',' + store.state.ztInfo.telNo + ',' + store.state.ztInfo.uid + ',1'
			return true
		} else {
			store.commit('Login', false)
			return false
		}
	} else {
		return false
	}
}
export default {}
