import {postFetch, getFetch} from '../http/api'

// 展唐登录接口
export const loginApi = async (params = {}) => {
	const result = await postFetch({
		url: '/apij/member/getToken',
		data: params
	})

	return result
}

export const getLoginVerifyCode = async (params = {}) => {
	const {
		telNo
	} = params
	const result = await postFetch({
		url: '/apij/member/createLoginSmsCode',
		data: {
			telNo
		}
	})

	return result
}
export const getGoodsDetails = async (params = {}) => {
	const result = await getFetch({
		url: '/api/member/findMine',
		data: params
	})

	return result
}
// 微信 code ,sessionkey,openid 换取
export const getWxLoginData = async (params = {}) => {
	const result = await getFetch({
		url: '/apij/wx/user/wx3ca04c4cbb30556e/login',
		data: params
	})

	return result
}
// 微信 手机号换取
export const getWxPhone = async (params = {}) => {
	const result = await getFetch({
		url: '/apij/wx/user/wx3ca04c4cbb30556e/phone',
		data: params
	})

	return result
}
// 微信 手机号换取
export const getWxInfo = async (params = {}) => {
	const result = await getFetch({
		url: '/apij/wx/user/wx3ca04c4cbb30556e/info',
		data: params
	})

	return result
}
// 展唐登录
export const ztLogin = async (params = {}) => {
	const result = await postFetch({
		url: '/apij/login/getToken',
		data: params
	})
	return result
}
// 展唐获取员工信息

export const ztStaffInfo = async (userId) => {
	const result = await getFetch({
		url: 'api/userInfo/findOne/' + userId
	})
	return result
}
// 更新展唐员工信息
export const upLoadztStaffInfo = async (params = {}) => {
	const result = await postFetch({
		url: 'api/userInfo/mobileUpdate',
		data: params
	})
	return result
}

// 展唐密码修改
export const upLoadztStaffpass = async (params = {}) => {
	const result = await postFetch({
		url: 'api/userInfo/updatePwd',
		data: params
	})
	return result
}

export default {}
