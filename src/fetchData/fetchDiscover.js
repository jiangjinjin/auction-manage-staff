import {postFetch, getFetch, deleteFetch} from '../http/api'

export const getSearchListApi = async (params = {}) => {
	const {
		goodsName = '',
		province = '',
		city = '',
		region = '',
		goodsTypeId = null,
		// state = null,
		// stateIn = '', 小程序这两个字段不用传
		courtId = '', // 法院id
		sorter = 'auction_time DESC',
		minPriceStart = '', // 起拍价-最小取值
		minPriceEnd = '', // 起拍价-最大取值
		pageSize = 20,
		currentPage = '1'
	} = params
	const result = await postFetch({
		url: '/api/goods/findKCPage',
		data: {
			goodsName,
			province,
			city,
			region,
			goodsTypeId,
			sorter,
			minPriceStart,
			minPriceEnd,
			courtId,
			pageSize,
			currentPage
		}
	})

	return result.list
}

export const getGoodsDetails = async (params = {}) => {
	const {
		goodsId
	} = params
	const result = await getFetch({
		url: `/apij/goods/findOneDetailApp/${goodsId}`
	})

	return result.data
}

export const modifyMapLocationApi = async (params = {}) => {
	const {
		id,
		lng,
		lat
	} = params
	const result = await postFetch({
		url: '/api/goods/updateSimple',
		data: {
			id,
			lng,
			lat
		}
	})

	return result
}

export const getCourtCityApi = async (params = {}) => {
	const result = await getFetch({
		url: '/apij/court/findCourtCity',
		data: params
	})

	return result.list
}

export const getCourtAllApi = async (params = {}) => {
	const {
		cityId
	} = params

	const result = await postFetch({
		url: '/apij/court/findAll',
		data: {
			cityId
		}
	})

	return result.list
}

export const deleteImgFileApi = async (params = {}) => {
	const {
		id
	} = params
	const result = await deleteFetch({
		url: `/api/goodsPath/delete/${id}`
	})

	return result
}

export default {}
