Component({
    externalClasses: ['i-class'],
    properties : {
        height : {
            type : String,
            value : '300'
        },
        itemHeight : {
            type : Number,
            value : 18
        }
    },
    relations : {
        '../index-item/index' : {
            type : 'child',
            linked(){
                this._updateDataChange();
            },
            linkChanged () {
                this._updateDataChange();
            },
            unlinked () {
                this._updateDataChange();
            }
        }
    },
    data : {
        relativeClientTop: 0, // 获取scrollview相对屏幕顶部具体
        scrollTop : 0,
        fixedData : [],
        current : 0,
        timer : null,
        startTop : 0,
        itemLength : 0,
        currentName : '',
        isTouches : false
    },

    ready() {
        this.queryMultipleNodes()
    },

    methods : {
        queryMultipleNodes: function(){
            const query = wx.createSelectorQuery().in(this)
            const $this = this
            query.select('.scrollView').boundingClientRect((res) => {
                $this.setData({
                    relativeClientTop: res.top,// 这个组件内 .scrollView 节点的上边界坐标
                })
            }).exec()
        },
        loop(){},
        _updateDataChange( ){
            const indexItems = this.getRelationNodes('../index-item/index');
            const len = indexItems.length;
            const fixedData = this.data.fixedData;
            /*
             * 使用函数节流限制重复去设置数组内容进而限制多次重复渲染
             * 暂时没有研究微信在渲染的时候是否会进行函数节流
            */
            if (len > 0) {

                if( this.data.timer ){
                    clearTimeout( this.data.timer )
                    this.setData({
                        timer : null
                    })
                }

                this.data.timer = setTimeout(()=>{
                    const data = [];
                    indexItems.forEach((item) => {
                        if( item.data.name && fixedData.indexOf( item.data.name ) === -1 ){
                            data.push(item.data.name);
                            item.updateDataChange();
                        }
                    })
                    this.setData({
                        fixedData : data,
                        itemLength : indexItems.length
                    })
                    //组件加载完成之后重新设置顶部高度
                    this.setTouchStartVal();
                },0);
                this.setData({
                    timer : this.data.timer
                })

            }
        },
        handlerScroll(event){
            const detail = event.detail;
            const scrollTop = detail.scrollTop;
            const indexItems = this.getRelationNodes('../index-item/index');
            indexItems.forEach((item,index)=>{
                let data = item.data;
                let offset = data.top + data.height;
                if( scrollTop < offset && scrollTop >= data.top ){
                    this.setData({
                        current : index,
                        currentName : data.currentName
                    })
                }
            })
        },
        getCurrentItem(index){
            const indexItems = this.getRelationNodes('../index-item/index');
            let result = {};
            result = indexItems[index].data;
            result.total = indexItems.length;
            result.firstTop = indexItems[0].data.top
            return result;
        },
        triggerCallback(options){
            this.triggerEvent('change',options)
        },
        handlerFixedTap(event){
            const eindex = event.currentTarget.dataset.index;
            const item = this.getCurrentItem(eindex);
            this.setData({
                scrollTop : item.top - this.data.relativeClientTop, // 减去相对视图顶部的高度 by jiangjinjin
                currentName : item.currentName,
                isTouches : true
            })
            this.triggerCallback({
                index : eindex,
                current : item.currentName,
                isTouches : true
            })
            // 隐藏toast by jiangjinjin
            setTimeout(() => {
                this.handlerTouchEnd()
            }, 150)
        },
        handlerTouchMove(event){
            const data = this.data;
            const touches = event.touches[0] || {};
            const pageY = touches.pageY;
            const rest = pageY - data.startTop;
            let index = Math.floor( rest/data.itemHeight );
            // 应该向下取整 by jiangjinjin
            // let index = Math.ceil( rest/data.itemHeight );
            if(index >= data.itemLength) {
                index = data.itemLength -1
            } else if(index < 0) {
                index = 0
            }
            // 此处在滑动时有问题 by jiangjinjin
            // index = index >= data.itemLength ? data.itemLength -1 : index;
            const movePosition = this.getCurrentItem(index);

           /*
            * 当touch选中的元素和当前currentName不相等的时候才震动一下
            * 微信震动事件
           */
            if( movePosition.name !== this.data.currentName ){
                wx.vibrateShort();
            }

            this.setData({
                scrollTop : movePosition.top - movePosition.firstTop, // 减去相对视图顶部的高度 by jiangjinjin
                currentName : movePosition.name,
                isTouches : true
            })

            this.triggerCallback({
                index : index,
                current : movePosition.name
            })
        },
        handlerTouchEnd(){
            this.setData({
                isTouches : false
            })
        },
        setTouchStartVal(){
            const className = '.i-index-fixed';
            const query = wx.createSelectorQuery().in(this);
            query.select( className ).boundingClientRect((res)=>{
                this.setData({
                    startTop : res.top
                })
            }).exec()
        }
    }
})
